<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<form id="productForm" method="post">
				
				<h2 class="form-signin-heading">Prodcut Form</h2>
				<div class="form-group">
					<label for="product_name" class="sr-only">Product Name</label>
					<input type="text" id="product_name" class="form-control" placeholder="Product Name" required autofocus>
				</div>
				<div class="form-group">
					<label for="quantity" class="sr-only">Quantity In Stock</label>
					<input type="text" id="quantity" class="form-control" placeholder="Quantity" required>
				</div>
				<div class="form-group">
					<label for="price_per_item" class="sr-only">Price/Item</label>
					<input type="text" id="price_per_item" class="form-control" placeholder="Price Per Item" required>
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
			</form>
		</div> <!-- /container -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script>
		$('#productForm').submit(function() {
			var request = $.ajax({
			  headers: {
            	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	  },
			  url: "saveProduct",
			  method: "POST",
			  data: {
	            product_name: $('#product_name').val(),
	            quantity: $('#quantity').val(),
	            price_per_item: $('#price_per_item').val(),
	        },
			  dataType: "html"
			});
			 
			request.done(function( msg ) {
			  $( "#log" ).html( msg );
			  
			});
			 
			request.fail(function( jqXHR, textStatus ) {
			  alert( "Request failed: " + textStatus );
			});
			return false;
		});
		</script>
	</body>
</html>