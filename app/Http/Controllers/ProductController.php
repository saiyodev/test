<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Storage;

class ProductController extends Controller
{
    //
    public function index() 
    {
    	return view('products.index', ['title' => 'Product Listing']);
    }

    public function saveProduct(Request $request)
    {
    	$productData = [];
    	$productData['Name'] = $request->input('product_name');
    	$productData['Quantity'] = $request->input('quantity');
    	$productData['Price Per Item'] = $request->input('price_per_item');
    	if(!Storage::disk('local')->exists('product.json')) {
    		$makeMultiArr[0] = $productData;
    		$data = json_encode($makeMultiArr);
    		Storage::put('product.json', $data);
    	} else {
    		$contents = Storage::get('product.json');
    		$contentArr = json_decode($contents);
    		$contentArr[] = $productData;
    		$data = json_encode($contentArr);
    		Storage::put('product.json', $data);
    	}

    }
}
